require 'test_helper'

class ModelGalleriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @model_gallery = model_galleries(:one)
  end

  test "should get index" do
    get model_galleries_url
    assert_response :success
  end

  test "should get new" do
    get new_model_gallery_url
    assert_response :success
  end

  test "should create model_gallery" do
    assert_difference('ModelGallery.count') do
      post model_galleries_url, params: { model_gallery: {  } }
    end

    assert_redirected_to model_gallery_url(ModelGallery.last)
  end

  test "should show model_gallery" do
    get model_gallery_url(@model_gallery)
    assert_response :success
  end

  test "should get edit" do
    get edit_model_gallery_url(@model_gallery)
    assert_response :success
  end

  test "should update model_gallery" do
    patch model_gallery_url(@model_gallery), params: { model_gallery: {  } }
    assert_redirected_to model_gallery_url(@model_gallery)
  end

  test "should destroy model_gallery" do
    assert_difference('ModelGallery.count', -1) do
      delete model_gallery_url(@model_gallery)
    end

    assert_redirected_to model_galleries_url
  end
end
