require 'test_helper'

class RelcoversControllerTest < ActionDispatch::IntegrationTest
  setup do
    @relcover = relcovers(:one)
  end

  test "should get index" do
    get relcovers_url
    assert_response :success
  end

  test "should get new" do
    get new_relcover_url
    assert_response :success
  end

  test "should create relcover" do
    assert_difference('Relcover.count') do
      post relcovers_url, params: { relcover: { sale_id: @relcover.sale_id } }
    end

    assert_redirected_to relcover_url(Relcover.last)
  end

  test "should show relcover" do
    get relcover_url(@relcover)
    assert_response :success
  end

  test "should get edit" do
    get edit_relcover_url(@relcover)
    assert_response :success
  end

  test "should update relcover" do
    patch relcover_url(@relcover), params: { relcover: { sale_id: @relcover.sale_id } }
    assert_redirected_to relcover_url(@relcover)
  end

  test "should destroy relcover" do
    assert_difference('Relcover.count', -1) do
      delete relcover_url(@relcover)
    end

    assert_redirected_to relcovers_url
  end
end
