require 'test_helper'

class Admin::TablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_table = admin_tables(:one)
  end

  test "should get index" do
    get admin_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_table_url
    assert_response :success
  end

  test "should create admin_table" do
    assert_difference('Admin::Table.count') do
      post admin_tables_url, params: { admin_table: { column1: @admin_table.column1, column2: @admin_table.column2, sale_id: @admin_table.sale_id } }
    end

    assert_redirected_to admin_table_url(Admin::Table.last)
  end

  test "should show admin_table" do
    get admin_table_url(@admin_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_table_url(@admin_table)
    assert_response :success
  end

  test "should update admin_table" do
    patch admin_table_url(@admin_table), params: { admin_table: { column1: @admin_table.column1, column2: @admin_table.column2, sale_id: @admin_table.sale_id } }
    assert_redirected_to admin_table_url(@admin_table)
  end

  test "should destroy admin_table" do
    assert_difference('Admin::Table.count', -1) do
      delete admin_table_url(@admin_table)
    end

    assert_redirected_to admin_tables_url
  end
end
