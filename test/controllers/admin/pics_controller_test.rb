require 'test_helper'

class Admin::PicsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_pic = admin_pics(:one)
  end

  test "should get index" do
    get admin_pics_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_pic_url
    assert_response :success
  end

  test "should create admin_pic" do
    assert_difference('Admin::Pic.count') do
      post admin_pics_url, params: { admin_pic: {  } }
    end

    assert_redirected_to admin_pic_url(Admin::Pic.last)
  end

  test "should show admin_pic" do
    get admin_pic_url(@admin_pic)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_pic_url(@admin_pic)
    assert_response :success
  end

  test "should update admin_pic" do
    patch admin_pic_url(@admin_pic), params: { admin_pic: {  } }
    assert_redirected_to admin_pic_url(@admin_pic)
  end

  test "should destroy admin_pic" do
    assert_difference('Admin::Pic.count', -1) do
      delete admin_pic_url(@admin_pic)
    end

    assert_redirected_to admin_pics_url
  end
end
