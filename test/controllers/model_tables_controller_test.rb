require 'test_helper'

class ModelTablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @model_table = model_tables(:one)
  end

  test "should get index" do
    get model_tables_url
    assert_response :success
  end

  test "should get new" do
    get new_model_table_url
    assert_response :success
  end

  test "should create model_table" do
    assert_difference('ModelTable.count') do
      post model_tables_url, params: { model_table: {  } }
    end

    assert_redirected_to model_table_url(ModelTable.last)
  end

  test "should show model_table" do
    get model_table_url(@model_table)
    assert_response :success
  end

  test "should get edit" do
    get edit_model_table_url(@model_table)
    assert_response :success
  end

  test "should update model_table" do
    patch model_table_url(@model_table), params: { model_table: {  } }
    assert_redirected_to model_table_url(@model_table)
  end

  test "should destroy model_table" do
    assert_difference('ModelTable.count', -1) do
      delete model_table_url(@model_table)
    end

    assert_redirected_to model_tables_url
  end
end
