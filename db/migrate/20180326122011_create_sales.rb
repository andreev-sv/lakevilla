class CreateSales < ActiveRecord::Migration[5.1]
  def change
    create_table :sales do |t|
      t.string :title
      t.text :text
      t.attachment :photo
      t.timestamps
    end
  end
end
