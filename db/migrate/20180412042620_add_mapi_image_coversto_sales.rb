class AddMapiImageCoverstoSales < ActiveRecord::Migration[5.1]
  def change
    add_attachment :sales, :map
    add_attachment :sales, :bimage
    add_attachment :sales, :simage
  end
end
