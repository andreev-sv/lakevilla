class ItemToGallery < ActiveRecord::Migration[5.1]
  def change
    add_column :galleries, :item, :string
  end
end
