class CreateAdminPics < ActiveRecord::Migration[5.1]
  def change
    create_table :pics do |t|
      t.text :text
      t.attachment :photo
      t.references :sale, foreign_key: true
      t.timestamps
    end
  end
end
