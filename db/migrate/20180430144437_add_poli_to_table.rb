class AddPoliToTable < ActiveRecord::Migration[5.1]
  def change
    add_column :tables, :content_id, :integer
    add_column :tables, :content_type, :string
    add_index :tables, [:content_type, :content_id]
  end
end
