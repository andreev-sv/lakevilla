class H3ToCover < ActiveRecord::Migration[5.1]
  def change
    add_column :covers, :text, :text
    add_column :covers, :subtitle, :string
    add_column :covers, :number, :integer
  end
end
