class CreateModels < ActiveRecord::Migration[5.1]
  def change
    create_table :models do |t|
      t.string :title
      t.text :text
      t.attachment :photo
      t.timestamps
    end
  end
end
