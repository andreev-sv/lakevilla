class CreateCovers < ActiveRecord::Migration[5.1]
  def change
    create_table :covers do |t|
      t.string :title
      t.references :model, foreign_key: true
      t.attachment :photo
      t.timestamps
    end
  end
end
