class NumberAndH2ToSheets < ActiveRecord::Migration[5.1]
  def change
        add_column :sheets, :number, :integer
        add_column :sheets, :subtitle, :string
  end
end
