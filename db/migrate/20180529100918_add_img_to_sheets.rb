class AddImgToSheets < ActiveRecord::Migration[5.1]
  def change
      add_attachment :sheets, :img1
      add_attachment :sheets, :img2
      add_attachment :sheets, :img3
      add_column :sheets, :string1, :string
      add_column :sheets, :string2, :string
      add_column :sheets, :string3, :string
  end
end
