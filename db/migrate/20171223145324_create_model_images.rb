class CreateModelImages < ActiveRecord::Migration[5.1]
  def change
    create_table :model_images do |t|
      t.attachment :photo
      t.references :model, foreign_key: true
      t.timestamps
    end
  end
end
