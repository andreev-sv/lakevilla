class CreateRelcovers < ActiveRecord::Migration[5.1]
  def change
    create_table :relcovers do |t|
      t.references :sale, foreign_key: true
        t.attachment :photo
        t.string :title
        t.string :subtitle
        t.text :text
        t.integer :item
      t.timestamps
    end
  end
end
