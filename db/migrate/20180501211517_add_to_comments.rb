class AddToComments < ActiveRecord::Migration[5.1]
  def change
    add_column :comments, :mail, :string
    add_column :comments, :phone, :integer
  end
end
