class ModelToPlans < ActiveRecord::Migration[5.1]
  def change
    add_attachment :models, :plan
    add_attachment :models, :simage
  end
end
