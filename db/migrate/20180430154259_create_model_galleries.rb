class CreateModelGalleries < ActiveRecord::Migration[5.1]
  def change
    create_table :model_galleries do |t|
      t.text :text
      t.attachment :photo
      t.references :model, foreign_key: true
      t.timestamps
    end
  end
end
