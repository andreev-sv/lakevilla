class CreateSmodels < ActiveRecord::Migration[5.1]
  def change
    create_table :smodels do |t|
      t.string :title
      t.attachment :photo
      t.references :sheet, foreign_key: true
      t.timestamps
    end
  end
end
