class CreateModelTables < ActiveRecord::Migration[5.1]
  def change
    create_table :model_tables do |t|
      t.string :column1
      t.string :column2
      t.references :model, foreign_key: true
      t.timestamps
    end
  end
end
