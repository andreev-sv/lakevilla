class CreateTables < ActiveRecord::Migration[5.1]
  def change
    create_table :tables do |t|
      t.string :column1
      t.string :column2
      t.references :sale, foreign_key: true

      t.timestamps
    end
  end
end
