class CreateModelPictures < ActiveRecord::Migration[5.1]
  def change
    create_table :model_pictures do |t|
      t.belongs_to :model, index: true
      t.datetime :published_at
      t.attachment :photo
      t.timestamps
    end
  end
end
