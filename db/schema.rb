# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180529101739) do

  create_table "comments", force: :cascade do |t|
    t.string "name"
    t.string "title"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "mail"
    t.integer "phone"
  end

  create_table "covers", force: :cascade do |t|
    t.string "title"
    t.integer "model_id"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "text"
    t.string "subtitle"
    t.integer "number"
    t.index ["model_id"], name: "index_covers_on_model_id"
  end

  create_table "galleries", force: :cascade do |t|
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "item"
  end

  create_table "model_galleries", force: :cascade do |t|
    t.text "text"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer "model_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["model_id"], name: "index_model_galleries_on_model_id"
  end

  create_table "model_images", force: :cascade do |t|
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer "model_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["model_id"], name: "index_model_images_on_model_id"
  end

  create_table "model_pictures", force: :cascade do |t|
    t.integer "model_id"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["model_id"], name: "index_model_pictures_on_model_id"
  end

  create_table "model_tables", force: :cascade do |t|
    t.string "column1"
    t.string "column2"
    t.integer "model_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["model_id"], name: "index_model_tables_on_model_id"
  end

  create_table "models", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "item"
    t.string "plan_file_name"
    t.string "plan_content_type"
    t.integer "plan_file_size"
    t.datetime "plan_updated_at"
    t.string "simage_file_name"
    t.string "simage_content_type"
    t.integer "simage_file_size"
    t.datetime "simage_updated_at"
    t.string "subitem"
  end

  create_table "pics", force: :cascade do |t|
    t.text "text"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer "sale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_id"], name: "index_pics_on_sale_id"
  end

  create_table "pictures", force: :cascade do |t|
    t.string "title"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "relcovers", force: :cascade do |t|
    t.integer "sale_id"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.string "title"
    t.string "subtitle"
    t.text "text"
    t.integer "item"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sale_id"], name: "index_relcovers_on_sale_id"
  end

  create_table "sales", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "desc"
    t.string "map_file_name"
    t.string "map_content_type"
    t.integer "map_file_size"
    t.datetime "map_updated_at"
    t.string "bimage_file_name"
    t.string "bimage_content_type"
    t.integer "bimage_file_size"
    t.datetime "bimage_updated_at"
    t.string "simage_file_name"
    t.string "simage_content_type"
    t.integer "simage_file_size"
    t.datetime "simage_updated_at"
    t.text "minitext"
  end

  create_table "sheets", force: :cascade do |t|
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.string "title"
    t.text "text"
    t.string "photo2_file_name"
    t.string "photo2_content_type"
    t.integer "photo2_file_size"
    t.datetime "photo2_updated_at"
    t.string "item"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "number"
    t.string "subtitle"
    t.string "img1_file_name"
    t.string "img1_content_type"
    t.integer "img1_file_size"
    t.datetime "img1_updated_at"
    t.string "img2_file_name"
    t.string "img2_content_type"
    t.integer "img2_file_size"
    t.datetime "img2_updated_at"
    t.string "img3_file_name"
    t.string "img3_content_type"
    t.integer "img3_file_size"
    t.datetime "img3_updated_at"
    t.string "string1"
    t.string "string2"
    t.string "string3"
  end

  create_table "smodels", force: :cascade do |t|
    t.string "title"
    t.string "photo_file_name"
    t.string "photo_content_type"
    t.integer "photo_file_size"
    t.datetime "photo_updated_at"
    t.integer "sheet_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sheet_id"], name: "index_smodels_on_sheet_id"
  end

  create_table "tables", force: :cascade do |t|
    t.string "column1"
    t.string "column2"
    t.integer "sale_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "content_id"
    t.string "content_type"
    t.index ["content_type", "content_id"], name: "index_tables_on_content_type_and_content_id"
    t.index ["sale_id"], name: "index_tables_on_sale_id"
  end

  create_table "tags", force: :cascade do |t|
    t.text "title"
    t.text "description"
    t.text "keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "item"
  end

end
