class Gallery < ApplicationRecord
  include Paperclip::Glue

  has_attached_file :photo, styles: { big: "1200x900>", medium: "400x300>", small: "100x75>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  validates :photo, attachment_presence: true
end
