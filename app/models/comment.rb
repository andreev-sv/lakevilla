class Comment < ApplicationRecord
  validates :name, :title, :text, presence: true
end
