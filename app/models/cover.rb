class Cover < ApplicationRecord
  include Paperclip::Glue
  belongs_to :model
  has_attached_file :photo, styles: { big: "1200x500>", medium: "600x375>", small: "500x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

end
