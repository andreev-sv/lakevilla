class ModelPicture < ApplicationRecord
  include Paperclip::Glue
  belongs_to :model
  has_attached_file :photo, styles: { big: "1200x500>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
end
