class Sheet < ApplicationRecord
  include Paperclip::Glue
  has_many :smodels, dependent: :destroy
  has_attached_file :photo, styles: { big: "1200x500>", medium: "1000x657>", small: "500x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  has_attached_file :img1, styles: { big: "1200x900>", medium: "800x600>", small: "400x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  has_attached_file :img2, styles: { big: "1200x900>", medium: "800x600>", small: "400x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  has_attached_file :img3, styles: { big: "1200x900>", medium: "800x600>", small: "400x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
end
