class Sale < ApplicationRecord
  include Paperclip::Glue
  has_many :tables
  has_many :relcovers
  has_many :pics
  has_attached_file :photo, styles: { big: "1200x500>", medium: "600x375>", small: "500x300>" }
  has_attached_file :map, styles: { big: "1200x500>", medium: "600x375>", small: "500x300>" }
  has_attached_file :bimage, styles: { big: "1200x500>", medium: "600x375>", small: "500x300>" }
  has_attached_file :simage, styles: { big: "1200x500>", medium: "1000x657>", small: "500x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  accepts_nested_attributes_for :tables, reject_if: proc { |attributes| attributes['column1'].blank? }
  accepts_nested_attributes_for :pics, reject_if: :all_blank
  accepts_nested_attributes_for :relcovers, reject_if: :all_blank
  validates :photo, attachment_presence: true
  validates :title, :text, presence: true
end
