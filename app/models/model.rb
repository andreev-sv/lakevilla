class Model < ApplicationRecord
  include Paperclip::Glue
  has_many :covers, dependent: :destroy
  has_many :model_images, dependent: :destroy
  has_many :model_tables, dependent: :destroy
  has_many :model_galleries, dependent: :destroy
  has_one :model_picture, dependent: :destroy
  accepts_nested_attributes_for :model_tables, reject_if: :all_blank
  accepts_nested_attributes_for :covers, reject_if: :all_blank
  has_attached_file :photo, styles: { big: "1200x500>", medium: "600x375>", small: "500x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  has_attached_file :simage, styles: { big: "1200x500>", medium: "600x375>", small: "500x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/
  has_attached_file :plan
  validates :photo, attachment_presence: true
  validates :title, presence: true
end
