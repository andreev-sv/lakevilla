class Pic < ApplicationRecord
  include Paperclip::Glue
  has_attached_file :photo, styles: { big: "1200x500>", medium: "600x375>", small: "500x300>" }
  validates_attachment_content_type :photo, content_type: /\Aimage\/.*\z/

  belongs_to :sale
end
