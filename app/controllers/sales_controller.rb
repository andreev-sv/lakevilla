class SalesController < ApplicationController
  def index
    @sales = Sale.all
  end
  def show
  @sale = Sale.find(params[:id])
  end
  private
  def sale_params
    params.require(:sale).permit(:title, :text, :minitext, :photo, :map, :bimage, :simage)
  end
end
