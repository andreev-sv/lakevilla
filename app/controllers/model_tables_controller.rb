class ModelTablesController < ApplicationController
  before_action :set_model_table, only: [:show, :edit, :update, :destroy]

  # GET /model_tables
  # GET /model_tables.json
  def index
    @model_tables = ModelTable.all
  end

  # GET /model_tables/1
  # GET /model_tables/1.json
  def show
  end

  # GET /model_tables/new
  def new
    @model_table = ModelTable.new
  end

  # GET /model_tables/1/edit
  def edit
  end

  # POST /model_tables
  # POST /model_tables.json
  def create
    @model_table = ModelTable.new(model_table_params)

    respond_to do |format|
      if @model_table.save
        format.html { redirect_to @model_table, notice: 'Model table was successfully created.' }
        format.json { render :show, status: :created, location: @model_table }
      else
        format.html { render :new }
        format.json { render json: @model_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /model_tables/1
  # PATCH/PUT /model_tables/1.json
  def update
    respond_to do |format|
      if @model_table.update(model_table_params)
        format.html { redirect_to @model_table, notice: 'Model table was successfully updated.' }
        format.json { render :show, status: :ok, location: @model_table }
      else
        format.html { render :edit }
        format.json { render json: @model_table.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /model_tables/1
  # DELETE /model_tables/1.json
  def destroy
    @model_table.destroy
    respond_to do |format|
      format.html { redirect_to model_tables_url, notice: 'Model table was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_model_table
      @model_table = ModelTable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def model_table_params
      params.fetch(:model_table, {})
    end
end
