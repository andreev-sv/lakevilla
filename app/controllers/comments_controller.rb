class CommentsController < ApplicationController
  def index
    @comments = Comment.all
  end
  def new
    @pictures = Picture.all
    @comment = Comment.new
    @sheets = Sheet.all
  end
  def create
    @comment = Comment.new(comment_params)
    @comment.save
  end
  private
  def comment_params
    params.require(:comment).permit(:name, :title, :text)
  end
end
