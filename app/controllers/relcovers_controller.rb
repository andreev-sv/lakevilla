class RelcoversController < ApplicationController
  def create
    @sale = Sale.find(params[:sale_id])
    @relcover = @sale.relcovers.create(relcover_params)
  end
  def show
    @sale = Sale.find(params[:sale_id])
    @relcover = @sale.relcovers.find(params[:id])
  end
  private
    def relcover_params
      params.require(:relcover).permit(:photo)
    end
end
