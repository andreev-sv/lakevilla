class ModelImagesController < ApplicationController
  def create
    @model = Model.find(params[:model_id])
    @model_image = @model.model_images.create(model_image_params)
  end
  def show
    @model = Model.find(params[:model_id])
    @model_image = @model.model_images.create(model_image_params)
  end
  private
    def model_image_params
      params.require(:model_image).permit(:photo)
    end
end
