class ModelsController < ApplicationController
  def index
    @models = Model.all
  end
  def show
  @model = Model.find(params[:id])
  end
  private
  def model_params
    params.require(:model).permit(:simage, :title, :text, :photo, model_picture_attributes: [ :photo ])
  end
end
