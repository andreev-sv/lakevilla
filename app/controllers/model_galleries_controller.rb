class ModelGalleriesController < ApplicationController
  def create
    @model = Model.find(params[:model_id])
    @model_gallery = @model.model_galleries.create(model_gallery_params)
  end
  def show
    @model = Model.find(params[:model_id])
    @model_gallery = @model.model_galleries.create(model_gallery_params)
  end
  private
    def model_gallery_params
      params.require(:model_gallery).permit(:photo)
    end
end
