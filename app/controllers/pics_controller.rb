class PicsController < ApplicationController
  def create
    @sale = Sale.find(params[:sale_id])
    @pic = @sale.pics.create(pic_params)
  end
  def show
    @sale = Sale.find(params[:sale_id])
    @pic = @sale.pics.find(params[:id])
  end
  private
    def pic_params
      params.require(:pic).permit(:photo)
    end
end
