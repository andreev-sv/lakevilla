class ModelPicturesController < ApplicationController
  def create
    @model = Model.find(params[:model_id])
    @model_picture = @model.model_pictures.create(model_picture_params)
  end
  def show
    @model = Model.find(params[:model_id])
    @model_picture = @model.model_pictures.create_(model_picture_params)
  end
  def index
    @model_pictures = ModelPicture.all
  end
  private
    def model_picture_params
      params.require(:model_picture).permit(:photo)
    end
end
