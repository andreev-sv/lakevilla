class StaticPagesController < ApplicationController
  def home
        @pictures = Picture.all
        @sheets = Sheet.all
        @models = Model.all
        @tags = Tag.all
        @sales = Sale.all
  end
  def lakevilla
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
  end
  def location
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
  end
  def genplan
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
    @sales = Sale.all

  end
  def infrastructure
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
  end
  def houses
    @models = Model.all
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
    @sales = Sale.all
  end
  def gallery
    @galleries = Gallery.all
    @pictures = Picture.all
    @tags = Tag.all
    @sheets = Sheet.all
  end
  def prices
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
  end
  def contacts
    @pictures = Picture.all
    @sheets = Sheet.all
    @comments = Comment.all
    @tags = Tag.all
  end
  def lakevilla
    @galleries = Gallery.all
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
  end
  private
  def sheet_params
    params.require(:sheet).permit(:title, :text, :photo, :item, :subtitle, :number, :img1, :img2, :img3, :string1, :string2, :string3)
  end
end
