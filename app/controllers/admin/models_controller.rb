class Admin::ModelsController < Admin::ApplicationController
  def index
    @models = Model.all
  end
  def new
    @model = Model.new
    @model.covers.build
    @model.model_tables.build
    @model.model_galleries.build
  end
  def show
  @model = Model.find(params[:id])
  end
  def edit
  @model = Model.find(params[:id])
  @model.covers.build
  @model.model_tables.build
  @model.model_galleries.build
  end
  def create
    @model = Model.new(model_params)
    if @model.save
      redirect_to admin_models_path
    else
      render :new
    end
  end
  def update
    @model = Model.find(params[:id])
    if @model.update(model_params)
    redirect_to admin_models_path
    else
    render 'edit'
    end
  end
  def destroy
  @model = Model.find(params[:id])
  @model.destroy
  redirect_to admin_models_path
  end
  private
  def model_params
    params.require(:model).permit(:subitem, :title, :text, :photo, :plan, :simage, :item, covers_attributes: [:title, :subtitle, :text, :photo, :id ], model_tables_attributes: [:column1, :column2, :id ], model_galleries_attributes: [:photo, :text, :id ])
  end
end
