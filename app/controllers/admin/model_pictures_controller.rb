class Admin::ModelPicturesController < Admin::ApplicationController
  def new
    @model_picture = ModelPicture.new
  end
  def show
  @model_picture = ModelPicture.find(params[:id])
  end
  def index
    @model_pictures = ModelPicture.all
  end
  def create
    @model_picture = ModelPicture.new(model_picture_params)
    @model_picture.save
  end
  def update
    @model_picture = ModelPicture.find(params[:id])
    @model.update(model_picture_params)
  end
  def destroy
  @model_picture = ModelPicture.find(params[:id])
  @model_picture.destroy
  redirect_to admin_model_pictures_path
  end
  private
  def model_picture_params
      params.require(:model_picture).permit(:photo)
  end
end
