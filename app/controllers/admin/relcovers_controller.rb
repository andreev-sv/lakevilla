class Admin::RelcoversController < Admin::ApplicationController
  def create
    @sale = Sale.find(params[:sale_id])
    @relcover = @sale.relcovers.create(relcover_params)
  end
  def show
    @sale = Sale.find(params[:sale_id])
    @relcover = @sale.relcovers.find(params[:id])
  end
  def update
    @sale = Sale.find(params[:sale_id])
    if @sale.update(relcovers_params)
      redirect_to admin_sale_path
    else
      render 'edit'
    end
  end
  def destroy
    @sale = Sale.find(params[:sale_id])
    @relcover = @sale.relcovers.find(params[:id])
    @relcover.destroy
    redirect_to admin_sale_path(@sale)
  end
  private
    def relcover_params
      params.require(:relcover).permit(:photo)
    end
end
