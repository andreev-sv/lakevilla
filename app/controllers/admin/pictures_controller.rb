class Admin::PicturesController < Admin::ApplicationController
  def index
    @pictures = Picture.all
  end
  def new
    @picture = Picture.new
  end
  def edit
    @picture = Picture.find(params[:id])
  end
  def create
    @picture = Picture.new(picture_params)
    @picture.save
    redirect_to root_path
  end
  def update
    @picture = Picture.find(params[:id])
    if @picture.update(picture_params)
    redirect_to admin_pictures_path
    else
    render 'edit'
    end
  end
  def destroy
  @picture = Picture.find(params[:id])
  @picture.destroy
  redirect_to admin_pictures_path
  end
  private
  def picture_params
    params.require(:picture).permit(:title, :text, :photo)
  end
end
