class Admin::SmodelsController < Admin::ApplicationController
  def create
    @sheet = Sheet.find(params[:sheet_id])
    @smodel = @sheet.smodels.create(smodel_params)
  end
  def show
    @sheet = Sheet.find(params[:sheet_id])
    @smodel = @sheet.smodels.find(params[:id])
  end
  def update
    @sheet = Sheet.find(params[:sheet_id])
    if @sheet.update(smodel_params)
    redirect_to admin_sheet_path(@sheet)
    else
    render 'edit'
    end
  end
  def destroy
  @sheet = Sheet.find(params[:sheet_id])
  @smodel = @sheet.smodels.find(params[:id])
  @smodel.destroy
  redirect_to admin_sheet_path(@sheet)
  end
  private
    def smodel_params
      params.require(:smodel).permit(:photo, :title)
    end
end
