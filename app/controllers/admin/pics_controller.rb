class Admin::PicsController < Admin::ApplicationController
  def create
    @sale = Sale.find(params[:sale_id])
    @pic = @sale.pics.create(pic_params)
  end
  def show
    @sale = Sale.find(params[:sale_id])
    @pic = @sale.pics.find(params[:id])
  end
  def update
    @sale = Sale.find(params[:sale_id])
    if @sale.update(pics_params)
    redirect_to admin_sale_path
    else
    render 'edit'
    end
  end
  def destroy
  @sale = Sale.find(params[:sale_id])
  @pic = @sale.pics.find(params[:id])
  @pic.destroy
  redirect_to admin_sale_path(@sale)
  end
  private
    def pic_params
      params.require(:pic).permit(:photo)
    end
end
