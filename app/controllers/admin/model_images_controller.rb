class Admin::ModelImagesController < Admin::ApplicationController
  def create
    @model = Model.find(params[:model_id])
    @model_image = @model.model_images.create(model_image_params)
  end
  def show
    @model = Model.find(params[:model_id])
    @model_image = @model.model_images.find(params[:id])
  end
  def update
    @model = Model.find(params[:model_id])
    if @model.update(model_image_params)
    redirect_to admin_models_path
    else
    render 'edit'
    end
  end
  def destroy
  @model = Model.find(params[:model_id])
  @model_image = @model.model_images.find(params[:id])
  @model_image.destroy
  redirect_to admin_model_path(@model)
  end
  private
    def model_image_params
      params.require(:model_image).permit(:photo)
    end
end
