class Admin::SalesController < Admin::ApplicationController
  def index
    @sales = Sale.all
  end
  def new
    @sale = Sale.new
    @sale.tables.build
    @sale.relcovers.build
    @sale.pics.build
  end
  def show
  @sale = Sale.find(params[:id])
  end
  def edit
  @sale = Sale.find(params[:id])
  @sale.tables.build
  @sale.relcovers.build
  @sale.pics.build
  end
  def create
    @sale = Sale.new(sale_params)
    if @sale.save
      redirect_to admin_sales_path
    else
      render :new
    end
  end
  def update
    @sale = Sale.find(params[:id])
    if @sale.update(sale_params)
    redirect_to admin_sales_path
    else
    render 'edit'
    end
  end
  def destroy
  @sale = Sale.find(params[:id])
  @sale.destroy
  redirect_to admin_sales_path
  end
  private
  def sale_params
    params.require(:sale).permit(:title, :desc, :text, :minitext, :photo, :map, :bimage, :simage, tables_attributes: [:column1, :column2, :id ], relcovers_attributes: [:title, :subtitle, :photo, :text, :id ], pics_attributes: [:photo, :text, :id ])
  end
end
