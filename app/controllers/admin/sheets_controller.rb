class Admin::SheetsController < Admin::ApplicationController
    def index
      @sheets = Sheet.all
    end
    def new
      @sheet = Sheet.new
    end
    def edit
      @sheet = Sheet.find(params[:id])
    end
    def create
      @sheet = Sheet.new(sheet_params)
      @sheet.save
      redirect_to root_path

    end
    def update
      @sheet = Sheet.find(params[:id])
      if @sheet.update(sheet_params)
      redirect_to admin_sheets_path
      else
      render 'edit'
      end
    end
    def destroy
    @sheet = Sheet.find(params[:id])
    @sheet.destroy
    redirect_to admin_sheet_path
    end
    private
    def sheet_params
      params.require(:sheet).permit(:title, :text, :photo, :item, :subtitle, :number, :img1, :img2, :img3, :string1, :string2, :string3)
    end
  end
