class Admin::CommentsController < Admin::ApplicationController
  def index
    @comments = Comment.all
    @sheets = Sheet.all
    @pictures = Picture.all
  end
  def new
    @comment = Comment.new
  end
  def create
    @comment = Comment.new(comment_params)
    @comment.save
  end
  def edit
  @comment = Comment.find(params[:id])
  end
  def update
    @comment = Comment.find(params[:id])
    if @comment.update(comment_params)
    redirect_to admin_comments_path
    else
    render 'edit'
    end
  end
  def destroy
  @comment = Comment.find(params[:id])
  @comment.destroy
  redirect_to admin_comments_path
  end
  private
  def comment_params
    params.require(:comment).permit(:name, :title, :text)
  end
end
