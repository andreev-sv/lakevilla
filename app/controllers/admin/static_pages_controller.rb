class Admin::StaticPagesController < Admin::ApplicationController
  def home
      @pictures = Picture.all
      @sheets = Sheet.all
      @models = Model.last(6)
  end
  def lakevilla
      @pictures = Picture.all
      @sheets = Sheet.all
  end
  def genplan
    @pictures = Picture.all
    @sheets = Sheet.all
  end
  def infrastructure
    @pictures = Picture.all
    @sheets = Sheet.all
  end
  def projects;
    @pictures = Picture.all
    @sheets = Sheet.all
  end
  def gallery
    @pictures = Picture.all
    @sheets = Sheet.all
    @galleries = Gallery.all
  end
  def prices
    @pictures = Picture.all
    @sheets = Sheet.all
  end
  def location
    @pictures = Picture.all
    @sheets = Sheet.all
  end
  def sale
    @pictures = Picture.all
    @sheets = Sheet.all
  end
  def contacts
    @pictures = Picture.all
    @sheets = Sheet.all
    @comments = Comment.all
    @tags = Tag.all
  end
  def houses
    @models = Model.all
    @pictures = Picture.all
    @sheets = Sheet.all
    @tags = Tag.all
    @sales = Sale.all
    
  end
  def index
    @pictures = Picture.all
  end
  def new
    @picture = Picture.new
  end
  def edit
  @picture = Picture.find(params[:id])
  @sheet = Sheet.find(params[:id])
  end
  def create
    @picture = Picture.new(picture_params)
    if @picture.save
      redirect_to admin_picture_path
    else
      render :new
    end
  end
  def update
    @picture = Picture.find(params[:id])
    if @picture.update(model_params)
    redirect_to admin_models_path
    else
    render 'edit'
    end
  end
end
