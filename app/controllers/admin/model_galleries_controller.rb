class Admin::ModelGalleriesController < Admin::ApplicationController
  def show
    @model = Model.find(params[:model_id])
    @model_gallery = @model.model_galleries.find(params[:id])
  end
  def create
    @model = Model.find(params[:model_id])
    @model_gallery = @model.model_galleries.create(model_gallery_params)
  end
  def destroy
  @model = Model.find(params[:model_id])
  @model_gallery = @model.model_galleries.find(params[:id])
  @model_gallery.destroy
  redirect_to admin_model_path(@model)
  end
  private
    def model_gallery_params
      params.require(:model_gallery).permit(:photo, :text)
    end
end
