Rails.application.routes.draw do
  get '/home' => 'static_pages#home'
  get '/lakevilla' => 'static_pages#lakevilla'
  get '/location' => 'static_pages#location'
  get '/genplan' => 'static_pages#genplan'
  get '/gallery' => 'static_pages#gallery'
  get '/prices' => 'static_pages#prices'
  get '/infrastructure' => 'static_pages#infrastructure'
  get '/houses' => 'static_pages#houses'

  get '/realization' => 'static_pages#realization'
  get '/indivdesign' =>  'static_pages#indivdesign'
  get '/finishedhouse' =>  'static_pages#finishedhouse'
  get '/buildingprocess' =>  'static_pages#buildingprocess'
  get '/improvement' =>  'static_pages#improvement'
  get '/lakevilla' =>  'static_pages#lakevilla'
  get '/kivarinruchey' =>  'static_pages#kivarinruchey'
  get '/zerkalniy' =>  'static_pages#zerkalniy'
  get '/contacts' => 'static_pages#contacts'
  root 'static_pages#home'
  resources :sales do
    resources :gallerysales
    resources :relcovers
    resources :pics
  end
  resources :sheets do
    resources :smodels
  end
  resources :comments
  resources :models do
    resources :model_images
    resources :model_picture
    resources :model_galleries
  end
  namespace :admin do
    resources :model_tables
    resources :sales do
      resources :relcovers
      resources :pics
    end
    resources :tags
    resources :covers
    resources :pictures
    resources :model_pictures
    resources :models do
      resources :model_images
      resources :model_galleries
    end
    resources :sheets do
      resources :smodels
    end
    resources :galleries
    resources :tables

    resources :comments
    get '/home' => 'static_pages#home'
    get '/lakevilla' => 'static_pages#lakevilla'
    get '/location' => 'static_pages#location'
    get '/genplan' => 'static_pages#genplan'
    get '/infrastructure' => 'static_pages#infrastructure'
    get '/houses' => 'static_pages#houses'
    root 'static_pages#home'
    get '/nordplan' => 'static_pages#nordplan'
    get '/building' => 'static_pages#building'
    get '/villages' => 'static_pages#villages'
    get '/gallery' => 'static_pages#gallery'
    get '/prices' => 'static_pages#prices'
    get '/earth' => 'static_pages#earth'
    get '/projects' => 'static_pages#projects'
    get '/sale' => 'static_pages#sale'
    get '/gazobeton' =>  'static_pages#gazobeton'
    get '/karkas' =>  'static_pages#karkas'
    get '/krovlya' =>  'static_pages#krovlya'
    get '/monolit' =>  'static_pages#monolit'
    get '/systems' =>  'static_pages#systems'
    get '/fundament' =>  'static_pages#fundament'
    get '/europeanprojects' =>  'static_pages#europeanprojects'
    get '/nordplanprojects' =>  'static_pages#nordplanprojects'
    get '/indivdesign' =>  'static_pages#indivdesign'
    get '/finishrdhouse' =>  'static_pages#finishrdhouse'
    get '/buildingprocess' =>  'static_pages#buildingprocess'
    get '/improvement' =>  'static_pages#improvement'
    get '/lakevilla' =>  'static_pages#lakevilla'
    get '/kivarinruchey' =>  'static_pages#kivarinruchey'
    get '/zerkalniy' =>  'static_pages#zerkalniy'
    get '/contacts' => 'static_pages#contacts'
    get '/realization' => 'static_pages#realization'
  end
end
